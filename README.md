# Green Tree Initiative Website

# Wireframe/Design
```
Project Adobe XD wireframes - http://bit.do/mbngo
```

# Features
```
Responsive:
        -Mobile
        -Tablet
        -Large Screen
```

# Tools
```
HTML
```
```
CSS
```
```
Live Server: 
This is a little development server with live reload capability.

Note:
Use it for hacking your HTML/JavaScript/CSS files
but not for deploying the final site.
```

# Resources
```
Project assets - http://bit.do/mbngo-assets
```


## Authors

* **Sumit Aswal**
